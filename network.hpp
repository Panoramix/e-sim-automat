#ifndef NETWORK_HPP
#define NETWORK_HPP
#include <curl/curl.h>
#include <string>

int intalize();
int finish();
std::string getpage(std::string page);
std::string postpage(std::string page, std::string postfields = "");
void savepage();
int writer(char *data, size_t size, size_t nmemb, std::string  *buffer);

#endif
