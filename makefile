SOURCES=$(wildcard *.cpp)
OBJECTS=$(SOURCES:.cpp=.o)
COMPILER=g++
#COMPILER=i586-mingw32msvc-g++

all: e-sim

e-sim: $(OBJECTS)
	$(COMPILER) -lcurl -o e-sim $(OBJECTS)

%.o: %.cpp %.hpp
	$(COMPILER) -c -o $@ $<

clean:
	rm *.o e-sim

