#include "network.hpp"

CURL *curl;
CURLcode res;

static char errorBuffer[CURL_ERROR_SIZE];
std::string buffer;

int intalize()
{
    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
        curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "cookie");
        curl_easy_setopt(curl, CURLOPT_COOKIEJAR, "cookie");
        return 0;
    }
    else return 1;
}

std::string postpage(std::string page, std::string postfields)
{
    buffer.clear();
    curl_easy_setopt(curl, CURLOPT_URL, page.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPPOST, 1L);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postfields.c_str());
    res = curl_easy_perform(curl);
    std::string to_return = buffer;
    return to_return;
}

std::string  getpage(std::string page)
{
    buffer.clear();
    curl_easy_setopt(curl, CURLOPT_URL, page.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
    res = curl_easy_perform(curl);
    std::string to_return = buffer;
    return to_return;
}


int writer(char *data, size_t size, size_t nmemb, std::string *buffer)
{
    int result = 0;
    if (buffer != NULL)
    {
        buffer->append(data, size * nmemb);
        result = size * nmemb;
    }
    return result;
}

int finish()
{
    curl_easy_cleanup(curl);
}
