#ifndef E_SIM_HPP
#define E_SIM_HPP
#include <string>
#include <iostream>
//#include <regex>
#include "network.hpp"


enum EsimServer {
    primera,
    secura
};

enum WorkStatus {
    WORK_OK,
    WORK_NO_WORKED_TODAY,
    WORK_ALREDY_WORKED,
    WORK_NO_JOB,
    WORK_NO_MONEY_IN_COMPANY,
    WORK_NO_RAW_IN_COMPANY
};

enum TrainStatus {
    TRAIN_OK,
    TRAIN_NO_TRAINED_TODAY,
    TRAIN_ALREDY_TRAINED
};

enum ProductType {
    IRON,
    GRAIN,
    OIL,
    STONE,
    WOOD,
    DIAMOND,
    WEAPON,
    HOUSE,
    GIFT,
    FOOD,
    TICKET,
    DEFENSE_SYSTEM,
    HOSPITAL,
    ESTATE
};



class Product
{
    private:
        ProductType type;
        unsigned int quality;
        unsigned int stock;
    public:
        Product();
};

class Food : Product
{
    private:
    public:
        Food();
        bool eat();
};


class Esim
{
private:
    std::string base_addres;
public:
    Esim (EsimServer s);
    //~Esim ();
    void openhomepage();
    void showbaseaddres();
    void login(std::string login, std::string password);
    WorkStatus work();
    void train();
    void do_tasks();
    void logout();
    EsimServer get_server();
};

#endif
