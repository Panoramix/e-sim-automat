#include "e-sim.hpp"

Esim::Esim(EsimServer s)
{
    switch(s)
    {
        case 0:
            base_addres= "http://primera.e-sim.org/";
            break;
        case 1:
            base_addres="http://secura.e-sim.org/";
            break;
    }
    intalize();
}

void Esim::openhomepage()
{
    getpage(base_addres);
}

void Esim::login(std::string login, std::string password)
{
    std::string l("login=");
    l.append(login);
    std::string p("&password=");
    p.append(password);
    std::string data = l + p;
    postpage(base_addres + "login.html",data);
}


WorkStatus Esim::work()
{
    std::string result = postpage(base_addres+"work.html","action=work");
    if (result.find("Todays work results") != std::string::npos)
    {
        return WORK_OK;
    }
    else if (result.find("There is no raw in the company.")  != std::string::npos)
    {
        return WORK_NO_RAW_IN_COMPANY;
    }
    else if (result.find("There is no money in the company.")  != std::string::npos)
    {
        return WORK_NO_MONEY_IN_COMPANY;
    }
}

void Esim::train()
{
    std::string result = postpage(base_addres+"train.html");
    std::size_t size= result.find_first_of("work");
    std::cout<<size;
}

void Esim::logout()
{
    getpage(base_addres+"logout.html");
    finish();
}

void Esim::showbaseaddres()
{
    std::cout<<base_addres<<std::endl;
}

void Esim::do_tasks()
{
    work();
    train();
}
